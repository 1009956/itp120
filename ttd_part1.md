# TDD
I don't know if I should be taking notes about the creation of the earth as we know it.

## Code rots
As a design rots, it becomes rigid, fragile, and immobile.
We don't clean code because we're afraid of breaking it.
### The cleanup
When we clean a module, we don't have a way of knowing it behaves properly. It *might* break or corrupt or worse. But this doesn't always happen.
We cannot clean the code up properly without eliminating fear of change.
### Demo
Remove comments that describe easily understandable things. Code should explain itself.
Try to shorten code while still making it understandable. The goal should be to remove redundancy.
When lines are getting way too long, you can hit enter in the middle of them? Might only work with expressions.
```html += isInline ? "" : endl;```?
```if (hasChildren())```
```html += "</" +  tagName() + ">";```
## Uncle Bob's three laws
### 1
You are not supposed to write any production code except to pass a failing test
### 2
Write only enough of a test to demonstrate a failure
### 3
Write only enough production code to pass the test

If you follow these three laws, you'll be ~~sooooo slow~~ a lot more efficient since as you write it, your code works. It is hard to debug for too long if you wrote the code a minute ago.
###### documentation bad seeing code good
The long sought goal of highly decoupled systems is trivial through the production of tests?! Uncle bob oh my god
If you can trust your tests, you can be unafraid to test your code. **The test stops the code from rotting.** It eliminates the fear of change.